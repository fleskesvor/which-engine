package main

import (
	"fleskesvor.com/which-engine"
	"fleskesvor.com/which-engine/internal/engine"
	"fleskesvor.com/which-engine/internal/utils"
	"fmt"
	flag "github.com/spf13/pflag"
	"os"
)

func main() {
	var multi bool
	var summary bool
	var path string

	flag.BoolVarP(&multi, "multi", "m", false, "multiple directory mode")
	flag.BoolVarP(&summary, "summary", "s", false, "print summary (when using -m flag)")
	flag.StringVarP(&path, "path", "p", "", "path to use as starting point")
	flag.Parse()

	startingPoint := "."
	if isValidDirectory(path) {
		startingPoint = path
	}

	if multi {
		multiple(startingPoint, summary)
	} else {
		single(startingPoint)
	}
}

func single(gameDir string) {
	currentDir := gameDir
	if gameDir == "." {
		currentDir = utils.GetCurrentDir()
	}
	getEngine(gameDir, currentDir)
}

func multiple(startingPoint string, printSummary bool) {
	totalDirs := 0
	summary := make(map[string]int)
	dirs := utils.FindDirs(startingPoint)

	if len(dirs) < 1 {
		fmt.Println("No game directories found")
		return
	}

	for _, dir := range utils.FindDirs(startingPoint) {
		ok, eng := getEngine(dir, dir)
		totalDirs++

		if !ok || !printSummary {
			continue
		}
		if _, exists := summary[eng.Key()]; exists {
			summary[eng.Key()]++
		} else {
			summary[eng.Key()] = 1
		}
	}

	if printSummary {
		fmt.Printf("\nSummary:\n\n")
		for key, val := range summary {
			fmt.Printf("    %-25s: %5d\n", key, val)
		}
		fmt.Printf("\n%-30s %5d\n\n", "Total:", totalDirs)
	}
}

func getEngine(gameDir, currentDir string) (bool, which.Engine) {
	if ok, eng, _ := engine.GetEngine(gameDir); ok {
		fmt.Printf("Found %s in %s\n", eng, currentDir)
		return ok, eng
	} else {
		fmt.Printf("Unable to determine engine in %s\n", currentDir)
		return ok, which.Engine{}
	}
}

func isValidDirectory(path string) bool {
	if path == "" {
		return false
	}
	info, err := os.Stat(path)
	return !os.IsNotExist(err) && info.IsDir()
}
