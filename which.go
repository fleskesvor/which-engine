package which

import (
	"fmt"
	"os"
	"strings"
)

type Engine struct {
	Name    string
	Runtime string
	Version string
}

type EngineType interface {
	GetEngine(dir string, paths []os.FileInfo) (bool, Engine, error)
}

func (engine Engine) String() string {
	parts := make([]string, 0)

	if engine.Name != "" {
		parts = append(parts, fmt.Sprintf("engine %s", engine.Name))
	}
	if engine.Runtime != "" {
		parts = append(parts, fmt.Sprintf("runtime %s", engine.Runtime))
	}
	if engine.Version != "" {
		parts = append(parts, fmt.Sprintf("version %s", engine.Version))
	}
	return strings.Join(parts, ", ")
}

func (engine Engine) Key() string {
	if engine.Name != "" {
		return engine.Name
	}
	return engine.Runtime
}
