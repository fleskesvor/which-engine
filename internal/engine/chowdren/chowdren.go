package chowdren

import (
	"fleskesvor.com/which-engine"
	"fleskesvor.com/which-engine/internal/utils"
	"os"
)

type Chowdren struct {
	Name    string
	Runtime string
	Version string
}

const runtime = "Chowdren"

func NewChowdren() Chowdren {
	return Chowdren{Runtime: runtime}
}

// Chowdren is a runtime for Clickteam Fusion and Scirra Construct: https://mp2.dk/chowdren/
// The original engine code is converted to native C++ code, so it might not be possible to
// determine which engine was used to develop the game.
func (chowdren Chowdren) GetEngine(dir string, paths []os.FileInfo) (bool, which.Engine, error) {
	if _, err := utils.FindFile(dir, "Chowdren", 1); err == nil {
		return true, which.Engine(chowdren), nil
	}
	return false, which.Engine{}, nil
}
