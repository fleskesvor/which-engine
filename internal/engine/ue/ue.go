package ue

import (
	"fleskesvor.com/which-engine"
	"fleskesvor.com/which-engine/internal/utils"
	"os"
)

type Ue struct {
	Name    string
	Runtime string
	Version string
}

const name = "Unreal Engine"

func NewUe() Ue {
	return Ue{Name: name}
}

// Unreal Engine games will have a directory named "Engine"
func (ue Ue) GetEngine(dir string, paths []os.FileInfo) (bool, which.Engine, error) {
	if utils.HasDirname("Engine", paths) {
		return true, which.Engine(ue), nil
	}
	return false, which.Engine{}, nil
}
