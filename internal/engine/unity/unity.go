package unity

import (
	"errors"
	"fleskesvor.com/which-engine"
	"fleskesvor.com/which-engine/internal/utils"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"unicode"
)

type Unity struct {
	Name    string
	Runtime string
	Version string
}

const name = "Unity"

func NewUnity() Unity {
	return Unity{Name: name}
}

func (unity Unity) withVersion(version string) which.Engine {
	unity.Version = version
	return which.Engine(unity)
}

// Unity games have a "*_Data" directory, which, among other things,
// contain a data file named "resources.assets"
func (unity Unity) GetEngine(dir string, paths []os.FileInfo) (bool, which.Engine, error) {
	dataDir := utils.FindDirSuffix("_Data", paths)
	if dataDir != nil {
		if version, err := getVersion(dir, dataDir.Name()); err == nil {
			return true, unity.withVersion(version), nil
		} else {
			return true, which.Engine(unity), err
		}
	}
	return false, which.Engine(unity), nil
}

// Unity uses a variation of SemVer on the format "major.minor.update",
// where "update" carries information about the release life cycle.
// A valid version might be eg. 5.6.7f1 or 2020.1.16f1.
func getVersion(gameDir, dataDir string) (string, error) {
	filename := filepath.Join(gameDir, dataDir, "resources.assets")
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return "", err
	}

	var str = ""
	for _, r := range string(data) {
		if isPrintable(r) {
			str += string(r)
		} else if len(str) >= 5 && str[0] >= '0' && str[0] <= '9' {
			return str, nil
		} else {
			str = ""
		}
	}
	return "", errors.New(fmt.Sprintf("Unable to determine version in %s", filename))
}

func isPrintable(r rune) bool {
	return r <= unicode.MaxASCII && unicode.IsPrint(r)
}
