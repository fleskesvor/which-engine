package ags

import (
	"fleskesvor.com/which-engine"
	"fleskesvor.com/which-engine/internal/utils"
	"os"
)

type Ags struct {
	Name    string
	Runtime string
	Version string
}

const name = "Adventure Game Studio"

func NewAgs() Ags {
	return Ags{Name: name}
}

// AGS games will have a file named "acsetup.cfg"
func (ags Ags) GetEngine(_ string, paths []os.FileInfo) (bool, which.Engine, error) {
	if utils.HasFilename("acsetup.cfg", paths) {
		return true, which.Engine(ags), nil
	}
	return false, which.Engine{}, nil
}
