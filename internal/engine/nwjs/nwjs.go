package nwjs

import (
	"fleskesvor.com/which-engine"
	"fleskesvor.com/which-engine/internal/utils"
	"os"
)

type Nwjs struct {
	Name, Runtime, Version string
}

const runtime = "NW.js"

const construct2Pattern = "<div id=\"c2canvasdiv\">"
const rpgMakerPattern = "src=\"js/rpg_core.js\""

func NewNwjs() Nwjs {
	return Nwjs{Runtime: runtime}
}

func (nwjs Nwjs) construct2() which.Engine {
	nwjs.Name = "Construct 2"
	return which.Engine(nwjs)
}

func (nwjs Nwjs) rpgMaker() which.Engine {
	nwjs.Name = "RPG Maker"
	return which.Engine(nwjs)
}

// NW.js are distributed in one of two ways as described on:
// https://docs.nwjs.io/en/latest/For%20Users/Package%20and%20Distribute/
// 1) Plain files with entrypoint specified in package.json
// 2) Zipped to a file named package.nw
// Since the nw executable is often renamed, we check for one of the other files
func (nwjs Nwjs) GetEngine(dir string, paths []os.FileInfo) (bool, which.Engine, error) {
	if filePath, err := utils.FindFile(dir, "package.nw", 0); err == nil {
		if ok, err := utils.FindTextInFileInZip(filePath, "index.html", construct2Pattern); ok {
			return true, nwjs.construct2(), err
		} else if ok, err := utils.FindTextInFileInZip(filePath, "index.html", rpgMakerPattern); ok {
			return true, nwjs.rpgMaker(), err
		}
		return true, which.Engine(nwjs), nil
	} else if utils.HasFilename("nw.pak", paths) {
		if filePath, err := utils.FindFile(dir, "index.html", -1); err == nil {
			if ok, err := utils.FindTextInFile(filePath, construct2Pattern); ok {
				return true, nwjs.construct2(), err
			} else if ok, err := utils.FindTextInFile(filePath, rpgMakerPattern); ok {
				return true, nwjs.rpgMaker(), err
			}
		}
		return true, which.Engine(nwjs), nil
	}
	return false, which.Engine{}, nil
}
