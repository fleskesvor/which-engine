package java

import (
	"fleskesvor.com/which-engine"
	"fleskesvor.com/which-engine/internal/utils"
	"os"
	"path/filepath"
)

type Java struct {
	Name    string
	Runtime string
	Version string
}

const runtime = "Java"

const versionPattern = "[\\d._]{5,}"

func NewJava() Java {
	return Java{Runtime: runtime}
}

func (java Java) withVersion(version string) which.Engine {
	java.Version = version
	return which.Engine(java)
}

// FIXME: Java games will have one or more *.jar files,
// which can be extracted to examine the "META-INF/MANIFEST.MF",
// to (sometimes) find the Java version.
func (java Java) GetEngine(dir string, paths []os.FileInfo) (bool, which.Engine, error) {
	jarFile := utils.FindFileSuffix(".jar", paths)
	if jarFile != nil {
		jarPath := filepath.Join(dir, jarFile.Name())
		if version, err := utils.FindPatternInFileInZip(jarPath, "META-INF/MANIFEST.MF", versionPattern); err == nil {
			return true, java.withVersion(version), nil
		} else {
			return true, which.Engine(java), err
		}
	}
	return false, which.Engine{}, nil
}
