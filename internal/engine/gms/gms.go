package gms

import (
	"fleskesvor.com/which-engine"
	"fleskesvor.com/which-engine/internal/utils"
	"os"
)

type Gms struct {
	Name    string
	Runtime string
	Version string
}

const name = "GameMaker Studio"

func NewGms() Gms {
	return Gms{Name: name}
}

// GameMaker games will have a file named "runner"
func (gms Gms) GetEngine(_ string, paths []os.FileInfo) (bool, which.Engine, error) {
	if utils.HasFilename("runner", paths) {
		return true, which.Engine(gms), nil
	}
	return false, which.Engine{}, nil
}
