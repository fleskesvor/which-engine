package engine

import (
	"fleskesvor.com/which-engine"
	"fleskesvor.com/which-engine/internal/engine/ags"
	"fleskesvor.com/which-engine/internal/engine/fna"
	"fleskesvor.com/which-engine/internal/engine/gms"
	"fleskesvor.com/which-engine/internal/engine/godot"
	"fleskesvor.com/which-engine/internal/engine/ue"
	"reflect"
	"testing"
)

func TestGetEngine(t *testing.T) {
	tests := []struct {
		name    string
		dir     string
		want    bool
		want1   which.Engine
		wantErr bool
	}{
		{
			"AGS",
			"testdata/games/ags-game",
			true,
			which.Engine(ags.NewAgs()),
			false,
		},
		{
			"FNA",
			"testdata/games/fna-game",
			true,
			which.Engine(fna.NewFna()),
			false,
		},
		{
			"GMS",
			"testdata/games/gms-game",
			true,
			which.Engine(gms.NewGms()),
			false,
		},
		{
			"Godot",
			"testdata/games/godot-game",
			true,
			which.Engine(godot.NewGodot()),
			false,
		},
		{
			"Java",
			"testdata/games/java-game",
			true,
			which.Engine{
				Runtime: "Java",
				Version: "1.8.0_275",
			},
			false,
		},
		{
			"UE",
			"testdata/games/ue-game",
			true,
			which.Engine(ue.NewUe()),
			false,
		},
		{
			"Unity",
			"testdata/games/unity-game",
			true,
			which.Engine{
				Name:    "Unity",
				Version: "2020.1.16f1",
			},
			false,
		},
		{
			"Chowdren",
			"testdata/games/chowdren-game",
			true,
			which.Engine{
				Runtime: "Chowdren",
			},
			false,
		},
		{
			"Construct 2",
			"testdata/games/c2-game",
			true,
			which.Engine{
				Name:    "Construct 2",
				Runtime: "NW.js",
			},
			false,
		},
		{
			"RPG Maker",
			"testdata/games/rpg-maker-game",
			true,
			which.Engine{
				Name:    "RPG Maker",
				Runtime: "NW.js",
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, err := GetEngine(tt.dir)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetEngine() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetEngine() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("GetEngine() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
