package engine

import (
	"fleskesvor.com/which-engine"
	"fleskesvor.com/which-engine/internal/engine/ags"
	"fleskesvor.com/which-engine/internal/engine/chowdren"
	"fleskesvor.com/which-engine/internal/engine/fna"
	"fleskesvor.com/which-engine/internal/engine/gms"
	"fleskesvor.com/which-engine/internal/engine/godot"
	"fleskesvor.com/which-engine/internal/engine/java"
	"fleskesvor.com/which-engine/internal/engine/nwjs"
	"fleskesvor.com/which-engine/internal/engine/ue"
	"fleskesvor.com/which-engine/internal/engine/unity"
	"io/ioutil"
)

// Ordered roughly by frequency on Itch.io:
// https://itch.io/game-development/engines/most-projects
var engineTypes = []which.EngineType{
	unity.NewUnity(),
	gms.NewGms(),
	nwjs.NewNwjs(),
	ue.NewUe(),
	godot.NewGodot(),
	java.NewJava(),
	ags.NewAgs(),
	fna.NewFna(),
	chowdren.NewChowdren(),
}

func GetEngine(dir string) (bool, which.Engine, error) {
	paths, err := ioutil.ReadDir(dir)
	if err != nil {
		return false, which.Engine{}, err
	}
	for _, engineType := range engineTypes {
		if ok, engine, err := engineType.GetEngine(dir, paths); ok {
			return true, engine, nil
		} else if err != nil {
			return false, which.Engine{}, err
		}
	}
	return false, which.Engine{}, nil
}
