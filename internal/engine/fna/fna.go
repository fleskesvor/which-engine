package fna

import (
	"fleskesvor.com/which-engine"
	"fleskesvor.com/which-engine/internal/utils"
	"os"
)

type Fna struct {
	Name    string
	Runtime string
	Version string
}

const name = "FNA"

func NewFna() Fna {
	return Fna{Name: name}
}

// FNA games will have a file named "FNA.dll"
func (fna Fna) GetEngine(_ string, paths []os.FileInfo) (bool, which.Engine, error) {
	if utils.HasFilename("FNA.dll", paths) {
		return true, which.Engine(fna), nil
	}
	return false, which.Engine{}, nil
}
