package godot

import (
	"fleskesvor.com/which-engine"
	"fleskesvor.com/which-engine/internal/utils"
	"os"
)

type Godot struct {
	Name    string
	Runtime string
	Version string
}

const name = "Godot"

func NewGodot() Godot {
	return Godot{Name: name}
}

// FIXME: Godot games will have a *.pck file (usually data.pck),
// which contains resource paths prefixed with "res://"
func (godot Godot) GetEngine(_ string, paths []os.FileInfo) (bool, which.Engine, error) {
	if utils.HasFileSuffix(".pck", paths) {
		return true, which.Engine(godot), nil
	}
	return false, which.Engine{}, nil
}
