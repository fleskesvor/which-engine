package utils

import (
	"archive/zip"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

func HasFilename(filename string, paths []os.FileInfo) bool {
	for _, path := range paths {
		if !path.IsDir() && path.Name() == filename {
			return true
		}
	}
	return false
}

func HasDirname(dirname string, paths []os.FileInfo) bool {
	for _, path := range paths {
		if path.IsDir() && path.Name() == dirname {
			return true
		}
	}
	return false
}

func HasFileSuffix(fileSuffix string, paths []os.FileInfo) bool {
	return FindFileSuffix(fileSuffix, paths) != nil
}

func FindFileSuffix(fileSuffix string, paths []os.FileInfo) os.FileInfo {
	for _, path := range paths {
		if !path.IsDir() && strings.HasSuffix(path.Name(), fileSuffix) {
			return path
		}
	}
	return nil
}

func FindFile(dir, filename string, maxDepth int) (string, error) {
	var filePath string

	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if skipDir(dir, path, info, maxDepth) {
			return filepath.SkipDir
		}
		if err != nil {
			return err
		}
		if !info.IsDir() && info.Name() == filename {
			filePath = path
			return io.EOF
		}
		return nil
	})
	if err == io.EOF {
		err = nil
	} else if err == nil {
		err = fmt.Errorf("file %s not found in %s", filename, dir)
	}
	return filePath, err
}

func skipDir(dir, path string, info os.FileInfo, maxDepth int) bool {
	if info.IsDir() && info.Name() == ".git" {
		return true
	}
	if maxDepth < 0 || !info.IsDir() {
		return false
	}
	depth := getDepth(path) - getDepth(dir)
	return depth > maxDepth
}

func getDepth(path string) int {
	if path == "." {
		return 0
	}
	return len(strings.Split(path, "/"))
}

func HasDirSuffix(dirSuffix string, paths []os.FileInfo) bool {
	return FindDirSuffix(dirSuffix, paths) != nil
}

func FindDirSuffix(dirSuffix string, paths []os.FileInfo) os.FileInfo {
	for _, path := range paths {
		if path.IsDir() && strings.HasSuffix(path.Name(), dirSuffix) {
			return path
		}
	}
	return nil
}

func FindDirs(startingPoint string) []string {
	var dirs []string
	paths, _ := ioutil.ReadDir(startingPoint)
	for _, path := range paths {
		if path.IsDir() {
			dirName := path.Name()
			if startingPoint != "." {
				dirName = filepath.Join(startingPoint, dirName)
			}
			dirs = append(dirs, dirName)
		}
	}
	return dirs
}

// Interface for matching a text or pattern string
type findMatch func([]byte, string) (bool, string)

// Implementation of findMatch interface for matching a text string
func findText(bytes []byte, text string) (bool, string) {
	return strings.Contains(string(bytes), text), ""
}

// Implementation of findMatch interface for matching a pattern string
func findPattern(bytes []byte, pattern string) (bool, string) {
	if match := regexp.MustCompile(pattern).FindString(string(bytes)); match != "" {
		return true, match
	}
	return false, ""
}

func FindTextInFileInZip(zipPath, filename, text string) (bool, error) {
	ok, _, err := findMatchInFileInZip(zipPath, filename, text, findText)
	return ok, err
}

func FindPatternInFileInZip(zipPath, filename, text string) (string, error) {
	_, match, err := findMatchInFileInZip(zipPath, filename, text, findPattern)
	return match, err
}

func findMatchInFileInZip(zipPath, filename, text string, findMatch findMatch) (bool, string, error) {
	var ok bool
	var match string

	zipReader, err := zip.OpenReader(zipPath)
	if err != nil {
		return false, "", err
	}
	defer zipReader.Close()

	var indexFile *zip.File

	for _, file := range zipReader.File {
		if file.Name == filename {
			indexFile = file
		}
	}

	if indexFile != nil {
		fileContents, err := indexFile.Open()
		if err != nil {
			return false, "", err
		}
		defer fileContents.Close()
		bytes, err := ioutil.ReadAll(fileContents)
		if err != nil {
			return false, "", err
		}
		// Evaluate file contents using a findMatch implementation
		ok, match = findMatch(bytes, text)
	}
	return ok, match, nil
}

func FindTextInFile(filePath, text string) (bool, error) {
	if data, err := ioutil.ReadFile(filePath); err == nil {
		return strings.Contains(string(data), text), nil
	} else {
		return false, err
	}
}

func GetCurrentDir() string {
	dir, _ := os.Getwd()
	parts := strings.Split(dir, "/")
	return parts[len(parts)-1]
}
