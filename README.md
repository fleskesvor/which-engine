# Which Engine

Attempt to detect which game engine a game was made with

## Download

The latest pre-built version is available for
[download here](https://gitlab.com/fleskesvor/which-engine/-/jobs/artifacts/master/file/which-engine?job=build).

## Usage

For ease of use, it is recommended to copy the binary to somewhere on your `$PATH`, eg. `/usr/local/bin`
or `~/.local/bin` (if applicable).

Then run the application from a game directory
```
which-engine
```

The output should look something like the output below:
```
Found engine Adventure Game Studio, version Unknown in ags-game
```

It is also possible to run the application from a directory containing multiple games in sub directories, eg. your
Steam app directory, by setting the `-m` flag
```
which-engine -m
```

The output should then look something like the output below:
```
Found engine Adventure Game Studio, version Unknown in ags-game
Found engine FNA, version Unknown in fna-game
Found engine GameMaker Studio, version Unknown in gms-game
Found engine Godot, version Unknown in godot-game
Found engine Java, version 1.8.0_275 in java-game
Found engine Unreal Engine, version Unknown in ue-game
Found engine Unity, version 2020.1.16f1 in unity-game
```

## Build

```
go build cmd/which-engine.go
```

## Test data

Test data for Unity can be created using `/dev/urandom`, eg.:
```bash
head -c 100 </dev/urandom > internal/engine/testdata/games/unity-game/Game_Data/resources.assets
echo "2020.1.16f1" >> internal/engine/testdata/games/unity-game/Game_Data/resources.assets
head -c 100 </dev/urandom >> internal/engine/testdata/games/unity-game/Game_Data/resources.assets
```
